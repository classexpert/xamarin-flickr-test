﻿using System.Runtime.Serialization;

namespace App4.Models
{
    [DataContract]
    public class Photo
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string owner { get; set; }
        [DataMember]
        public string secret { get; set; }
        [DataMember]
        public string server { get; set; }
        [DataMember]
        public int farm { get; set; }
        [DataMember]
        public string title { get; set; }
        [DataMember]
        public int ispublic { get; set; }
        [DataMember]
        public int isfriend { get; set; }
        [DataMember]
        public int isfamily { get; set; }
        [DataMember]
        public Description description { get; set; }
        [DataMember]
        public string dateupload { get; set; }
        [DataMember]
        public string ownername { get; set; }
        [DataMember]
        public string tags { get; set; }
        [DataMember]
        public string originalsecret { get; set; }
        [DataMember]
        public string originalformat { get; set; }
        [DataMember]
        public double latitude { get; set; }
        [DataMember]
        public double longitude { get; set; }
        [DataMember]
        public int accuracy { get; set; }
        [DataMember]
        public int context { get; set; }
        [DataMember]
        public string place_id { get; set; }
        [DataMember]
        public string woeid { get; set; }
        [DataMember]
        public int geo_is_family { get; set; }
        [DataMember]
        public int geo_is_friend { get; set; }
        [DataMember]
        public int geo_is_contact { get; set; }
        [DataMember]
        public int geo_is_public { get; set; }

        public string info
        {
            get
            {
                if (!string.IsNullOrEmpty(title)) return title;
                return string.IsNullOrEmpty(tags) ? id : tags;
            }
        }
    }
}