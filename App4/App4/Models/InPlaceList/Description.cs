﻿using System.Runtime.Serialization;

namespace App4.Models
{
    [DataContract]
    public class Description
    {
        [DataMember]
        public string _content { get; set; }
    }
}