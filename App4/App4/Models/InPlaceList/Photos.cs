﻿using System.Collections.Generic;

namespace App4.Models
{
    public class Photos
    {
        public int page { get; set; }
        public string pages { get; set; }
        public int perpage { get; set; }
        public string total { get; set; }
        public List<Photo> photo { get; set; }
    }
}