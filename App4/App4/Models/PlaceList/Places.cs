﻿using System.Collections.Generic;

namespace App4.Models
{
    public class Places
    {
        public List<Place> place { get; set; }
        public string query { get; set; }
        public int total { get; set; }
    }
}