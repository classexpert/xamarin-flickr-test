﻿using System.Runtime.Serialization;

namespace App4.Models
{
    [DataContract]
    public class Place
    {
        [DataMember]
        public string place_id { get; set; }
        [DataMember]
        public string woeid { get; set; }
        [DataMember]
        public double latitude { get; set; }
        [DataMember]
        public double longitude { get; set; }
        [DataMember]
        public string place_url { get; set; }
        [DataMember]
        public string place_type { get; set; }
        [DataMember]
        public int place_type_id { get; set; }
        [DataMember]
        public string timezone { get; set; }
        [DataMember]
        public string _content { get; set; }
        [DataMember]
        public string woe_name { get; set; }
    }
}