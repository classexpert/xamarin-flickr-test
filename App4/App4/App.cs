﻿using System.Diagnostics;
using App4.Data;
using App4.Views;
using Xamarin.Forms;

namespace App4
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            MainPage = new StartPage();
        }

        protected override void OnStart()
        {
            Debug.WriteLine("START");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("SLEEP");
            CachedData.Save();
            // Handle when your app sleeps
        }


        protected override void OnResume()
        {
            Debug.WriteLine("RESUME");
            // Handle when your app resumes
        }
    }
}