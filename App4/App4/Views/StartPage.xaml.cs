﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App4.Data;
using App4.ViewModels;
using Xamarin.Forms;

namespace App4.Views
{
    public partial class StartPage : TabbedPage
    {
        public StartPage()
        {
            InitializeComponent();
            this.BindingContext = new StartViewModel(Navigation);
        }

    }
}
