﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App4.ViewModels;
using Xamarin.Forms;

namespace App4.Views
{
    public partial class PhotosInPlacePage : ContentPage
    {
        public PhotosInPlacePage(string placeId)
        {
            InitializeComponent();
            BindingContext = new PhotosInPlaceViewModel(this.Navigation,placeId);
        }
    }
}
