﻿using App4.ViewModels;
using Xamarin.Forms;

namespace App4.Views
{
    public partial class PhotoPage : ContentPage
    {
        public PhotoPage(string url)
        {
            InitializeComponent();
            BindingContext = new PhotoViewModel(url);
        }
    }
}