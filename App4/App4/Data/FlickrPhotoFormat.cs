﻿namespace App4.Data
{
    public enum FlickrPhotoFormat
    {
        Square = 1, // thumbnail
        Large = 2, // normal size
        Original = 64 // high resolution
    };
}