﻿namespace App4.Data
{
    public interface IDataHelper
    {
        void SaveData(string key, object data);
        T LoadData<T>(string key) where T : class;

        bool Exists(string key);
    }
}