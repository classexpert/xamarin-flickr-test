﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using App4.Models;
using Newtonsoft.Json;

namespace App4.Data
{
    public class FlickrFetcher
    {
        private const string FlickrAPIKey = "f1a61cc3b470706a399fa5e680a5ffed";

        private static readonly FlickrFetcher _instance = new FlickrFetcher();

        private FlickrFetcher()
        {
        }

        public static FlickrFetcher Api
        {
            get { return _instance; }
        }

        private string URLForQuery(string baseUrl)
        {
            return string.Format("{0}&format=json&nojsoncallback=1&api_key={1}", baseUrl, FlickrAPIKey);
        }

        private string URLforTopPlaces()
        {
            return URLForQuery(
                "https://api.flickr.com/services/rest/?method=flickr.places.getTopPlacesList&place_type_id=7");
        }

        private string URLforMoscowPlaces()
        {
            return URLForQuery(
                "https://api.flickr.com/services/rest/?method=flickr.places.find&query=Moscow");
        }

        private string URLforPhotosInPlace(string flickrPlaceId, int maxResults = 20)
        {
            return
                URLForQuery(
                    string.Format(
                        "https://api.flickr.com/services/rest/?method=flickr.photos.search&place_id={0}&per_page={1}&extras=original_format,tags,description,geo,date_upload,owner_name,place_url",
                        flickrPlaceId, maxResults));
        }

        public string UrlStringForPhoto(FlickrPhotoFormat format, Photo photo)
        {
            string formatString = "s";
            switch (format)
            {
                case FlickrPhotoFormat.Large:
                    formatString = "b";
                    break;
                case FlickrPhotoFormat.Original:
                    formatString = "o";
                    break;
            }

            return string.Format("http://farm{0}.static.flickr.com/{1}/{2}_{3}_{4}.{5}", photo.farm, photo.server,
                photo.id, photo.secret, formatString, "jpg");
        }

        public string URLforInformationAboutPlace(string flickrPlaceId)
        {
            return
                URLForQuery(
                    string.Format("https://api.flickr.com/services/rest/?method=flickr.places.getInfo&place_id={0}",
                        flickrPlaceId));
        }

        public async Task<List<Place>> GetPlacesList()
        {
            using (var httpClient = new HttpClient())
            {
                string jsonResult = await httpClient.GetStringAsync(URLforTopPlaces());
                var root = JsonConvert.DeserializeObject<PlacesResult>(jsonResult);
                Debug.WriteLine(root.places.place.Count);
                return root.places.place;
            }
        }

        public async Task<List<Photo>> GetInPlacesList(string flickrPlaceId)
        {
            using (var httpClient = new HttpClient())
            {
                string jsonResult = await httpClient.GetStringAsync(URLforPhotosInPlace(flickrPlaceId));
                var root = JsonConvert.DeserializeObject<PhotosResult>(jsonResult);
                Debug.WriteLine(root.photos.total);
                return root.photos.photo;
            }
        }
    }
}