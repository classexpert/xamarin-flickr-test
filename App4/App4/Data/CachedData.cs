﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using App4.Models;
using Xamarin.Forms;

namespace App4.Data
{
    public static class CachedData
    {
        private static IDataHelper _data = DependencyService.Get<IDataHelper>();  // Application.Current.Properties не подходит

        private static ObservableCollection<Photo> _lastImages;
        public static ObservableCollection<Photo> LastImages
        {
            get
            {
                if (_lastImages == null)
                {
                    if (_data.Exists("LastImages"))
                        _lastImages = _data.LoadData<ObservableCollection<Photo>>("LastImages");
                    else
                        _lastImages = new ObservableCollection<Photo>();

                }
                return _lastImages;
            }
            set
            {
                _lastImages = value;
                _data.SaveData("LastImages", value);
            }
        }

        private static List<Place> _places;
        public static List<Place> Places
        {
            get
            {
                if (_places == null)
                {
                    if (_data.Exists("Places"))
                        _places = _data.LoadData<List<Place>>("Places");
                    else
                        _places = new List<Place>();

                }
                return _places;
            }
            set
            {
                _places = value;
                _data.SaveData("Places", value);
            }
        }

        private static Dictionary<string, List<Photo>> _inPlacePhotos;

        public static Dictionary<string, List<Photo>> InPlacePhotos
        {
            get
            {
                if (_inPlacePhotos == null)
                {
                    if (_data.Exists("InPlacePhotos"))
                        _inPlacePhotos = _data.LoadData < Dictionary<string, List<Photo>>>("InPlacePhotos");
                    else
                        _inPlacePhotos = new Dictionary<string, List<Photo>>();
                }
                return _inPlacePhotos;
            }
            set { _inPlacePhotos = value; }
        }
 
        public static void Save()
        {
            Debug.WriteLine("--------------------- CachedData Save");
            _data.SaveData("LastImages", _lastImages);
            _data.SaveData("Places", Places);
            _data.SaveData("InPlacePhotos", InPlacePhotos);
        }


    }
}