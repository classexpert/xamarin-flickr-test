﻿using System.Collections.Generic;
using System.Diagnostics;
using App4.Data;
using App4.Models;
using App4.Views;
using Connectivity.Plugin;
using Xamarin.Forms;

namespace App4.ViewModels
{
    public class PhotosInPlaceViewModel : BaseViewModel
    {
        private readonly INavigation _navigation;
        private readonly string _placeId;
        private List<Photo> _items;

        private Photo _selectedPlace;

        public PhotosInPlaceViewModel(INavigation navigation, string placeId)
        {
            _navigation = navigation;
            _placeId = placeId;
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                if (args.IsConnected)
                    UpdateList(); //  Считаем, что за время отсутствия интернета можем потерять обновленные данные, даже если уже их получали
            };
            UpdateList();
        }

        public Photo SelectedPlace
        {
            get { return _selectedPlace; }
            set
            {
                if (_selectedPlace != value)
                {
                    _selectedPlace = value;
                    PlaceSelected();
                }
            }
        }

        public List<Photo> Items
        {
            get
            {
                if (_items == null && CachedData.InPlacePhotos.ContainsKey(_placeId))
                    _items = CachedData.InPlacePhotos[_placeId];
                return _items; 
            }
            protected set
            {
                _items = value;
                CachedData.InPlacePhotos[_placeId] = value;
                OnPropertyChanged();
            }
        }

        private async void UpdateList()
        {
            Debug.WriteLine(" UpdateImagesList");
            if (CrossConnectivity.Current.IsConnected)
                Items = await FlickrFetcher.Api.GetInPlacesList(_placeId);
        }

        private async void PlaceSelected()
        {
            string url = FlickrFetcher.Api.UrlStringForPhoto(FlickrPhotoFormat.Large, SelectedPlace);
            MessagingCenter.Send(this, string.Empty, SelectedPlace);
            Debug.WriteLine("ItemSelected " + SelectedPlace.info + " " + url);
            await _navigation.PushModalAsync(new PhotoPage(url));
        }
    }
}