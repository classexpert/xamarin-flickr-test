﻿using Connectivity.Plugin;

namespace App4.ViewModels
{
    public class PhotoViewModel : BaseViewModel
    {
        public PhotoViewModel(string url)
        {
            ImageUrl = url;
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                if (args.IsConnected)
                    OnPropertyChanged("ImageUrl");
            };
        }

        public string ImageUrl { get; protected set; }
    }
}