﻿using System.Collections.Generic;
using System.Diagnostics;
using App4.Data;
using App4.Models;
using App4.Views;
using Connectivity.Plugin;
using Xamarin.Forms;

namespace App4.ViewModels
{
    public class PlacesViewModel : BaseViewModel
    {
        private Place _selectedPlace;
        public Place SelectedPlace
        {
            get
            {
                return _selectedPlace;
            }
            set
            {
                if (_selectedPlace != value)
                {
                    _selectedPlace = value;
                    PlaceSelected();

                }
            }
        }

        private INavigation _navigation;
        public PlacesViewModel(INavigation navigation)
        {
            _navigation = navigation;
            UpdateImagesList();
                        
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                if (args.IsConnected)
                    UpdateImagesList(); //  Считаем, что за время отсутствия интернета можем потерять обновленные данные, даже если уже их получали
            };
        }

        public string Title
        {
            get { return "Top"; }
        }

        public List<Place> Items
        {
            get { return CachedData.Places; }
            protected set
            {
                CachedData.Places = value;
                OnPropertyChanged();
            }
        }

        private async void UpdateImagesList()
        {
            if (CrossConnectivity.Current.IsConnected)   // or NetworkInterface.GetIsNetworkAvailable()
                Items = await FlickrFetcher.Api.GetPlacesList();
        }

        private async void PlaceSelected()
        {
            Debug.WriteLine("ItemSelected " + SelectedPlace._content);
            await _navigation.PushModalAsync(new PhotosInPlacePage(SelectedPlace.place_id));
        }
    }
}