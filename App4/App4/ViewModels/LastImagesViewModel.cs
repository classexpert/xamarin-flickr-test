﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using App4.Data;
using App4.Models;
using App4.Views;
using Xamarin.Forms;
using Connectivity.Plugin;

namespace App4.ViewModels
{
    public class LastImagesViewModel : BaseViewModel
    {
        private readonly INavigation _navigation;
        public ObservableCollection<Photo> Items
        {
            get { return CachedData.LastImages; }
            protected set
            {
                CachedData.LastImages = value;
                OnPropertyChanged();
            }
        }

        private Photo _selectedPlace;

        public Photo SelectedPlace
        {
            get { return _selectedPlace; }
            set
            {
                if (_selectedPlace != value)
                {
                    _selectedPlace = value;
                    PlaceSelected();
                }
            }
        }

        public string Title
        {
            get { return "Last"; }
        }

        public LastImagesViewModel(INavigation navigation)
        {
            _navigation = navigation;
            ButtonClearCommand = new Command(ClearList);
            MessagingCenter.Subscribe<PhotosInPlaceViewModel, Photo>(this, string.Empty, (sender, arg) => Add(arg));
        }

    
        public ICommand ButtonClearCommand { protected set; get; }

        private void ClearList()
        {
            Debug.WriteLine("ClearList");
            Items.Clear();
        }

        public void Add(Photo photo)
        {
            if(Items.Count == 20)
                Items.RemoveAt(Items.Count-1);
            if(!Items.Any(e=>e.id==photo.id))
                Items.Insert(0, photo);
        }

        private async void PlaceSelected()
        {
            string url = FlickrFetcher.Api.UrlStringForPhoto(FlickrPhotoFormat.Large, SelectedPlace);
            Debug.WriteLine("ItemSelected " + SelectedPlace.info + " " + url);
            await _navigation.PushModalAsync(new PhotoPage(url));
        }
    }
}