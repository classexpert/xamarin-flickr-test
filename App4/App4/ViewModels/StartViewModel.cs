﻿using Xamarin.Forms;

namespace App4.ViewModels
{
    public class StartViewModel : BaseViewModel
    {
        private readonly LastImagesViewModel _lastImagesVM;
        private readonly PlacesViewModel _placesVM;
        private INavigation _navigation;

        public StartViewModel(INavigation navigation)
        {
            _navigation = navigation;
            _placesVM = new PlacesViewModel(_navigation);
            _lastImagesVM = new LastImagesViewModel(_navigation);
        }

        public PlacesViewModel PlacesVM
        {
            get { return _placesVM; }
        }

        public LastImagesViewModel LastImagesVM
        {
            get { return _lastImagesVM; }
        }
    }
}