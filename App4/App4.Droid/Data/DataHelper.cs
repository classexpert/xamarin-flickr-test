using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using App4.Data;
using App4.Droid.Data;
using App4.Droid.Extensions;
using Xamarin.Forms;

[assembly: Dependency(typeof (DataHelper))]

namespace App4.Droid.Data
{
    public class DataHelper : IDataHelper
    {
        public void SaveData(string key, object data)
        {
            Debug.WriteLine("SaveData Android");
            SaveObjectAsync(key, data);
        }

        public T LoadData<T>(string key) where T : class
        {
            Debug.WriteLine("LoadData Android " + key);
            if (Exists(key))
                return ReadObject<T>(key);
            return null;
        }

        public bool Exists(string fullpath)
        {
            using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                return store.FileExists(fullpath);
            }
        }

        public T ReadObject<T>(string fullpath)
        {
            using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (IsolatedStorageFileStream file = store.OpenFile(fullpath, FileMode.Open))
                {
                    using (var reader = new StreamReader(file))
                    {
                        string data = reader.ReadToEnd();
                        return data.Deserialize<T>();
                    }
                }
            }
        }

        public void SaveObjectAsync(string name, object data)
        {
            using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                using (var writeFile = new StreamWriter(store.OpenFile(name, FileMode.Create, FileAccess.Write)))
                {
                    writeFile.WriteLine(data.Serialize());
                }
            }
        }
    }
}