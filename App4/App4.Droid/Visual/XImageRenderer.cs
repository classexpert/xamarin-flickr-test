using Android.Views;
using App4.Controls;
using App4.Droid.Visual;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof (XImage), typeof (XImageRenderer))]

namespace App4.Droid.Visual
{
    /// <summary>
    /// Customizing Controls for Each Platform : http://developer.xamarin.com/guides/cross-platform/xamarin-forms/custom-renderer/ for custom Gesture
    /// </summary>
    public class XImageRenderer : ImageRenderer
    {
        private GestureDetector _detector;

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement == null)
            {
                GenericMotion -= HandleGenericMotion;
                Touch -= HandleTouch;
            }

            if (e.OldElement == null)
            {
                _detector = new GestureDetector(new XImageGestureListener(Control));
                GenericMotion += HandleGenericMotion;
                Touch += HandleTouch;
            }
        }

        private void HandleTouch(object sender, TouchEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

        private void HandleGenericMotion(object sender, GenericMotionEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }
    }
}