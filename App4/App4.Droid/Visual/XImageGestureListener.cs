using System;
using Android.Views;
using Android.Widget;

namespace App4.Droid.Visual
{
    public class XImageGestureListener : GestureDetector.SimpleOnGestureListener
    {
        private readonly ImageView _image;
        private int _sX;
        private int _sY;

        public XImageGestureListener(ImageView image)
        {
            _image = image;
        }

        public override bool OnDoubleTapEvent(MotionEvent e)
        {
            if (e.Action == MotionEventActions.Down)
            {
                if (_image.ScaleX <= 1f)
                {
                    _image.ScaleX = 2f;
                    _image.ScaleY = 2f;
                }
                else
                {
                    _image.ScaleX = 1f;
                    _image.ScaleY = 1f;
                }
            }
            return false;
        }

        public override bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            _sX += (int) distanceX;
            _sY += (int) distanceY;
            _image.ScrollTo(_sX, _sY);
            return false;
        }
    }
}