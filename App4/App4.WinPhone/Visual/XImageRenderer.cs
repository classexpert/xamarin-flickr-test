﻿using System.Windows.Input;
using System.Windows.Media;
using App4.Controls;
using App4.WinPhone.Visual;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;

[assembly: ExportRenderer(typeof (XImage), typeof (XImageRenderer))]

namespace App4.WinPhone.Visual
{
    /// <summary>
    /// Customizing Controls for Each Platform : http://developer.xamarin.com/guides/cross-platform/xamarin-forms/custom-renderer/ for custom Gesture
    /// </summary>
    public class XImageRenderer : ImageRenderer
    {
        private double _scaleX = 1;
        private double _scaleY = 1;

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                Control.RenderTransform = new CompositeTransform();
                Control.ManipulationStarted += Control_ManipulationStarted;
                Control.ManipulationDelta += Control_ManipulationDelta;
            }
        }

        private void Control_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            var transform = (Control.RenderTransform as CompositeTransform);
            transform.TranslateX += e.DeltaManipulation.Translation.X*transform.ScaleX;
            transform.TranslateY += e.DeltaManipulation.Translation.Y*transform.ScaleX;

            if (e.PinchManipulation != null)
            {
                if ((transform.ScaleX > 2 && e.PinchManipulation.CumulativeScale > 1) ||
                    (transform.ScaleY < 0.5 && e.PinchManipulation.CumulativeScale < 1))
                    return;

                transform.CenterX = e.PinchManipulation.Original.Center.X;
                transform.CenterY = e.PinchManipulation.Original.Center.Y;

                transform.ScaleX = _scaleX*e.PinchManipulation.CumulativeScale;
                transform.ScaleY = _scaleY*e.PinchManipulation.CumulativeScale;
            }
        }

        private void Control_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            var transform = (CompositeTransform) Control.RenderTransform;
            _scaleX = transform.ScaleX;
            _scaleY = transform.ScaleY;
        }

    }
}