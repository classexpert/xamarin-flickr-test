﻿using System.Diagnostics;
using System.IO.IsolatedStorage;
using App4.Data;
using App4.WinPhone.Data;
using Xamarin.Forms;

[assembly: Dependency(typeof (DataHelper))]

namespace App4.WinPhone.Data
{
    public class DataHelper : IDataHelper
    {
        public void SaveData(string key, object data)
        {
            IsolatedStorageSettings.ApplicationSettings[key] = data;
            Debug.WriteLine("SaveData WP " + key);
        }

        public T LoadData<T>(string key) where T : class
        {
            Debug.WriteLine("LoadData WP" + key);
            if (Exists(key))
                return (T) IsolatedStorageSettings.ApplicationSettings[key];
            return null;
        }

        public bool Exists(string key)
        {
            return IsolatedStorageSettings.ApplicationSettings.Contains(key);
        }
    }
}